﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using System.Reflection;
using System.Threading;
using System.Globalization;

namespace GraphEditor
{
    public partial class GraphForm : Form
    {
        private const int SIZE = 40;

        private Color color;
        private int selected;
        private bool moving;
        private Point start;
        private Graph graph;

        public GraphForm()
        {
            InitializeComponent();
            color = Color.Black;
            selected = -1;
            moving = false;
            graph = new Graph(SIZE);
            graphPictureBox.Paint += DrawAll;

        }

        #region Vertex functions

        private void AddVertex(int x, int y)
        {
            Graphics graphics = graphPictureBox.CreateGraphics();
            Vertex vertex = new Vertex(x, y, color, graph.Count);
            graph.AddVertex(vertex);
            DrawVertex(graphics, vertex);
        }

        private void RemoveVertex()
        {
            if (selected != -1)
            {
                graph.RemoveVertex(selected);
                selected = -1;
                buttonRemove.Enabled = false;
                graphPictureBox.Refresh();
            }
        }

        private void MoveVertex(Point mouse)
        {
            if (selected != -1)
            {
                graph[selected].MoveVertex((Size)(mouse - (Size)start));
                start = mouse;
                graphPictureBox.Refresh();
            }
        }

        #endregion


        #region Draw functions 

        private void DrawVertex(Graphics graphics, Vertex v, bool isSelected = false)
        {
            Rectangle rect = new Rectangle(v.X - SIZE / 2, v.Y - SIZE / 2, SIZE, SIZE);

            Brush brush = new SolidBrush(Color.White);
            graphics.FillEllipse(brush, rect);

            Pen pen = new Pen(v.color, 3);
            if (isSelected) pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            graphics.DrawEllipse(pen, rect);

            TextFormatFlags flags = TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter;
            TextRenderer.DrawText(graphics, v.Number.ToString(), this.Font, rect, v.color, flags);
        }

        private void DrawEdge(Graphics graphics, Vertex from, Vertex to)
        {
            Pen pen = new Pen(Color.Black, 3);
            graphics.DrawLine(pen, from.Crd, to.Crd);
        }

        private void DrawAll(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;

            foreach (Vertex from in graph)
            {
                foreach (Vertex to in from.BiggerNeighbors())
                {
                    DrawEdge(graphics, from, to);
                }
            }

            for (int i = 0; i < graph.Count; i++)
            {
                DrawVertex(graphics, graph[i], i == selected);
            }

        }

        #endregion


        private void GraphForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemoveVertex();
            }
        }

        #region Mouse functions

        private void graphPictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            int clicked = graph.FindVertex(e.X, e.Y);
            if (e.Button == MouseButtons.Left)
            {
                //if you clicked at point which is not in range of any existing vertex
                //you can place new vertex there
                if (clicked == -1)
                {
                    AddVertex(e.X, e.Y);
                }
                //if you clicked at existing vertex and selected vertex exists
                //you can remove or add edge
                else if (selected != -1)
                {
                    if (!graph.RemoveEdge(selected, clicked))
                    {
                        graph.AddEdge(selected, clicked);
                    }

                    graphPictureBox.Refresh();
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                selected = clicked;
                buttonRemove.Enabled = selected != -1;
                graphPictureBox.Refresh();
            }
        }

        private void graphPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            //start moving
            if (e.Button == MouseButtons.Middle)
            {
                moving = true;
                start = e.Location;
            }
        }

        private void graphPictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            //stop moving
            if (e.Button == MouseButtons.Middle)
            {
                moving = false;
                if (selected != -1)
                {
                    graph[selected].AdjustVertex(graphPictureBox.Right, graphPictureBox.Bottom);
                    graphPictureBox.Refresh();
                }
            }
        }

        private void graphPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (moving)
            {
                MoveVertex(e.Location);
            }
        }

        #endregion


        private void buttonColor_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                color = colorDialog.Color;
                buttonColorView.BackColor = color;
                if (selected != -1)
                {
                    graph[selected].color = colorDialog.Color;
                    graphPictureBox.Refresh();
                }
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            RemoveVertex();
        }

        private void buttonClean_Click(object sender, EventArgs e)
        {
            graph.Clean();
            graphPictureBox.Refresh();
        }


        private void buttonPolish_Click(object sender, EventArgs e)
        {
            ChangeLanguage("pl-PL");
        }

        private void buttonEnglish_Click(object sender, EventArgs e)
        {
            ChangeLanguage("en-US");
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Graph files (*.graph)| *.graph";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (var stream = saveFileDialog.OpenFile())
                {
                    if (stream == null)
                    {
                        MessageBox.Show("Error, file doesn't exist");
                    }
                    else
                    {
                        StreamWriter streamWriter = new StreamWriter(stream);
                        streamWriter.Write(graph.Serialize());
                        streamWriter.Close();
                    }
                    MessageBox.Show(Message.Save);
                }
            }

        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Pliki graf (*.graph)| *.graph";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (StreamReader streamReader = new StreamReader(openFileDialog.FileName))
                    {
                        string str = streamReader.ReadToEnd();
                        Graph graph = new Graph(SIZE);
                        graph.Deserialize(str);
                        this.graph = graph;
                        graphPictureBox.Refresh();
                        MessageBox.Show(Message.Load);
                    }
                } catch (Exception)
                {
                    MessageBox.Show(Message.Error);
                }

            }
        }


        private void ChangeLanguage(string lang)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);
            var manager = new ComponentResourceManager(this.GetType());


            ApplyResources(this, manager);

            var clientSize = this.ClientSize;
            manager.ApplyResources(this, "$this");
            this.ClientSize = clientSize;
        }

        private void ApplyResources(Control control, ComponentResourceManager manager)
        {
            var size = control.Size;
            var location = control.Location;
            manager.ApplyResources(control, control.Name);
            control.Size = size;
            control.Location = location;

            foreach (Control child in control.Controls)
            {
                ApplyResources(child, manager);
            }

        }

    }
}
