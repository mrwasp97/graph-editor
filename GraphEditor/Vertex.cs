﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphEditor
{
    internal class Vertex
    {
        public List<Vertex> neighbors;
        private Point crd;
        public Color color;
        private int number;

        public int Degree
        {
            get
            {
                return neighbors.Count;
            }
        }

        public int X
        {
            get
            {
                return crd.X;
            }
        }
        public int Y
        {
            get
            {
                return crd.Y;
            }
        }
        public Point Crd
        {
            get
            {
                return crd;
            }
        }

        public int Number
        {
            get
            {
                return number;
            }
        }

        public Vertex(int x, int y, Color color, int number)
        {
            neighbors = new List<Vertex>();
            this.crd = new Point(x, y);
            this.color = color;
            this.number = number;
        }

        public void DecrementNumber()
        {
            number--;
        }

        public void MoveVertex(Size dist)
        {
            crd += dist;
        }

        public void AdjustVertex(int maxX, int maxY)
        {
            if (crd.X < 0) crd.X = 0;
            if (crd.X > maxX) crd.X = maxX;
            if (crd.Y < 0) crd.Y = 0;
            if (crd.Y > maxY) crd.Y = maxY;
        }


        public void AddNeighbor(Vertex v)
        {
            neighbors.Add(v);
        }

        public bool RemoveNeighbor(int n)
        {
            for (int i = 0; i < neighbors.Count; i++)
            {
                if (neighbors[i].number == n)
                {
                    neighbors.RemoveAt(i);
                    return true;
                }
            }

            return false;
        }

        public IEnumerable<Vertex> BiggerNeighbors()
        {
            foreach (Vertex v in neighbors)
            {
                if (number < v.number)
                {
                    yield return v;
                }
            }
        }

        public string Serialize()
        {
            return $"{X},{Y},{color.ToArgb()}";
        }

        public static Vertex Deserialize(string str, int number)
        {
            string[] values = str.Split(',');
            int x = int.Parse(values[0]);
            int y = int.Parse(values[1]);
            int argb = int.Parse(values[2]);
            Color color = Color.FromArgb(argb);
            return new Vertex(x, y, color, number);
        }

    }
}
