﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GraphEditor
{
    internal class Graph : IEnumerable<Vertex>
    {
        private List<Vertex> vertices;
        private readonly int vertexSize;

        public Vertex this[int i]
        {
            get
            {
                return vertices[i];
            }
        }

        public int Count
        {
            get
            {
                return vertices.Count;
            }
        }

        public Graph(int vertexSize)
        {
            this.vertices = new List<Vertex>();
            this.vertexSize = vertexSize;
        }

        public void Clean()
        {
            vertices.Clear();
        }

        public int FindVertex(int x, int y)
        {
            int found = -1;
            int best = int.MaxValue;

            for (int i = 0; i < Count; i++)
            {
                int x1 = vertices[i].X - x;
                int y1 = vertices[i].Y - y;
                int dist = x1 * x1 + y1 * y1;

                if (dist <= vertexSize * vertexSize && dist < best)
                {
                    best = dist;
                    found = i;
                }
            }
            return found;
        }

        public void AddVertex(Vertex vertex)
        {
            vertices.Add(vertex);
        }

        public void RemoveVertex(int n)
        {
            foreach (Vertex neighbor in vertices[n].neighbors)
            {
                neighbor.RemoveNeighbor(n);
            }

            for (int i = n + 1; i < vertices.Count; i++)
            {
                vertices[i].DecrementNumber();
            }

            vertices.RemoveAt(n);
        }

        public void AddEdge(int fromIndex, int toIndex)
        {
            vertices[fromIndex].AddNeighbor(vertices[toIndex]);
            vertices[toIndex].AddNeighbor(vertices[fromIndex]);
        }

        public bool RemoveEdge(int fromIndex, int toIndex)
        {
            Vertex from = vertices[fromIndex];
            Vertex to = vertices[toIndex];

            if (from.RemoveNeighbor(toIndex))
            {
                to.RemoveNeighbor(fromIndex);
                return true;
            }

            return false;
        }

        public IEnumerator<Vertex> GetEnumerator()
        {
            foreach (Vertex v in vertices)
            {
                yield return v;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (Vertex v in vertices)
            {
                yield return v;
            }
        }

        public string Serialize()
        {
            StringBuilder str = new StringBuilder();
            foreach (Vertex v in vertices)
            {
                str.AppendLine(v.Serialize());
            }

            foreach (Vertex from in vertices)
            {
                foreach (Vertex to in from.BiggerNeighbors())
                {
                    str.AppendLine($"{from.Number},{to.Number}");
                }
            }

            return str.ToString();
        }

        public void Deserialize(string str)
        {
            using (StringReader reader = new StringReader(str))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] elems = line.Split(',');
                    if (elems.Length == 3)
                    {
                        AddVertex(Vertex.Deserialize(line, vertices.Count));
                    }
                    else if (elems.Length == 2)
                    {
                        int from = int.Parse(elems[0]);
                        int to = int.Parse(elems[1]);
                        AddEdge(from, to);
                    }
                    else throw new Exception();
                }
            }
        }
    }
}
