﻿namespace GraphEditor
{
    partial class GraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GraphForm));
            this.graphPictureBox = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxEdition = new System.Windows.Forms.GroupBox();
            this.buttonClean = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonColorView = new System.Windows.Forms.Button();
            this.buttonColor = new System.Windows.Forms.Button();
            this.groupBoxLanguage = new System.Windows.Forms.GroupBox();
            this.buttonEnglish = new System.Windows.Forms.Button();
            this.buttonPolish = new System.Windows.Forms.Button();
            this.groupBoxInEx = new System.Windows.Forms.GroupBox();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.graphPictureBox)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBoxEdition.SuspendLayout();
            this.groupBoxLanguage.SuspendLayout();
            this.groupBoxInEx.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // graphPictureBox
            // 
            resources.ApplyResources(this.graphPictureBox, "graphPictureBox");
            this.graphPictureBox.Name = "graphPictureBox";
            this.graphPictureBox.TabStop = false;
            this.graphPictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.graphPictureBox_MouseClick);
            this.graphPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.graphPictureBox_MouseDown);
            this.graphPictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.graphPictureBox_MouseMove);
            this.graphPictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.graphPictureBox_MouseUp);
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.groupBoxEdition, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBoxLanguage, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupBoxInEx, 0, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // groupBoxEdition
            // 
            this.groupBoxEdition.Controls.Add(this.buttonClean);
            this.groupBoxEdition.Controls.Add(this.buttonRemove);
            this.groupBoxEdition.Controls.Add(this.buttonColorView);
            this.groupBoxEdition.Controls.Add(this.buttonColor);
            resources.ApplyResources(this.groupBoxEdition, "groupBoxEdition");
            this.groupBoxEdition.Name = "groupBoxEdition";
            this.groupBoxEdition.TabStop = false;
            // 
            // buttonClean
            // 
            resources.ApplyResources(this.buttonClean, "buttonClean");
            this.buttonClean.Name = "buttonClean";
            this.buttonClean.UseVisualStyleBackColor = true;
            this.buttonClean.Click += new System.EventHandler(this.buttonClean_Click);
            // 
            // buttonRemove
            // 
            resources.ApplyResources(this.buttonRemove, "buttonRemove");
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonColorView
            // 
            resources.ApplyResources(this.buttonColorView, "buttonColorView");
            this.buttonColorView.BackColor = System.Drawing.Color.Black;
            this.buttonColorView.Name = "buttonColorView";
            this.buttonColorView.UseVisualStyleBackColor = false;
            // 
            // buttonColor
            // 
            resources.ApplyResources(this.buttonColor, "buttonColor");
            this.buttonColor.Name = "buttonColor";
            this.buttonColor.UseVisualStyleBackColor = true;
            this.buttonColor.Click += new System.EventHandler(this.buttonColor_Click);
            // 
            // groupBoxLanguage
            // 
            this.groupBoxLanguage.Controls.Add(this.buttonEnglish);
            this.groupBoxLanguage.Controls.Add(this.buttonPolish);
            resources.ApplyResources(this.groupBoxLanguage, "groupBoxLanguage");
            this.groupBoxLanguage.Name = "groupBoxLanguage";
            this.groupBoxLanguage.TabStop = false;
            // 
            // buttonEnglish
            // 
            resources.ApplyResources(this.buttonEnglish, "buttonEnglish");
            this.buttonEnglish.Name = "buttonEnglish";
            this.buttonEnglish.UseVisualStyleBackColor = true;
            this.buttonEnglish.Click += new System.EventHandler(this.buttonEnglish_Click);
            // 
            // buttonPolish
            // 
            resources.ApplyResources(this.buttonPolish, "buttonPolish");
            this.buttonPolish.Name = "buttonPolish";
            this.buttonPolish.UseVisualStyleBackColor = true;
            this.buttonPolish.Click += new System.EventHandler(this.buttonPolish_Click);
            // 
            // groupBoxInEx
            // 
            this.groupBoxInEx.Controls.Add(this.buttonLoad);
            this.groupBoxInEx.Controls.Add(this.buttonSave);
            resources.ApplyResources(this.groupBoxInEx, "groupBoxInEx");
            this.groupBoxInEx.Name = "groupBoxInEx";
            this.groupBoxInEx.TabStop = false;
            // 
            // buttonLoad
            // 
            resources.ApplyResources(this.buttonLoad, "buttonLoad");
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // buttonSave
            // 
            resources.ApplyResources(this.buttonSave, "buttonSave");
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.graphPictureBox, 0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // GraphForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.KeyPreview = true;
            this.Name = "GraphForm";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GraphForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.graphPictureBox)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBoxEdition.ResumeLayout(false);
            this.groupBoxLanguage.ResumeLayout(false);
            this.groupBoxInEx.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox graphPictureBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBoxEdition;
        private System.Windows.Forms.Button buttonColor;
        private System.Windows.Forms.GroupBox groupBoxLanguage;
        private System.Windows.Forms.Button buttonEnglish;
        private System.Windows.Forms.Button buttonPolish;
        private System.Windows.Forms.GroupBox groupBoxInEx;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonColorView;
        private System.Windows.Forms.Button buttonClean;
        private System.Windows.Forms.Button buttonRemove;
    }
}

